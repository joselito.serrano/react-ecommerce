import React, { createContext, useContext, useReducer } from "react";

const CartContext = createContext({
  cart: [],
  dispatch: () => {
    throw new Error("dispatch function must be overridden");
  },
});

const reducer = (state, action) => {
  // For debugging
  console.log(action);

  switch (action.type) {
    case "addToCart": {
      const foundIndex = state.cart.findIndex(
        (ci) => ci.id === action.product.id
      );

      const isAlreadyInCart = foundIndex >= 0;

      if (isAlreadyInCart) {
        return {
          cart: state.cart.map((cartItem, index) =>
            index === foundIndex
              ? { ...cartItem, quantity: cartItem.quantity + 1 }
              : cartItem
          ),
        };
      } else {
        const cartItem = { ...action.product, quantity: 1 };
        return { cart: state.cart.concat(cartItem) };
      }
    }

    case "clearCart": {
      return { cart: [] };
    }

    case "increaseQuantity": {
      const updatedCart = state.cart.map((cartItem) =>
        cartItem.id === action.productId
          ? { ...cartItem, quantity: cartItem.quantity + 1 }
          : cartItem
      );
      return { cart: updatedCart };
    }

    case "decreaseQuantity": {
      const updatedCart = state.cart.map((cartItem) =>
        cartItem.id === action.productId
          ? { ...cartItem, quantity: cartItem.quantity - 1 }
          : cartItem
      );
      return { cart: updatedCart };
    }

    case "removeFromCart": {
      const updatedCart = state.cart.filter((cartItem) => cartItem.id !== action.productId);
    return { cart: updatedCart };
    }

    default:
      throw new Error("Unhandled action type");
  }
};

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, { cart: [] });

  return (
    <CartContext.Provider value={{ cart: state.cart, dispatch }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCartContext = () => useContext(CartContext);
