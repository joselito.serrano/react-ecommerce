import React, { useEffect, useRef, useState } from "react";
import { useLocation } from "wouter";
import { useCartContext } from "../contexts/CartContext";

export default function Home() {
  const [_, setLocation] = useLocation();
  const [products, setProducts] = useState([]);
  const { dispatch } = useCartContext();

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => setProducts(json));
  }, []);

  const [filterProduct, setFilterProduct] = useState("");
  const filterProductRef = useRef();
  const [searchProductsStatus, setSearchProductsStatus] = useState(false);
  const [filterProductHolder, setFilterProductHolder] = useState("");

  const filterProductTextChanged = (e) => {
    setFilterProduct(e.target.value);

    if (searchProductsStatus && !e.target.value.trim()) {
      setFilterProductHolder("");
      setSearchProductsStatus(false);
    }
  };

  const handleFilterProduct = () => {
    setFilterProductHolder(filterProduct);
    setSearchProductsStatus(true);
  };

  const filteredProducts = searchProductsStatus
    ? products.filter((myProduct) =>
        myProduct.title.includes(filterProductHolder)
      )
    : products;
  // console.log();
  // console.log("searchProductsStatus:" + searchProductsStatus);
  // console.log("filterProductHolder:" + filterProductHolder);
  // console.log("filterProduct:" + filterProduct);
  // console.log();

  return (
    <>
      <h2>Home</h2>
      <input
        type="text"
        value={filterProduct}
        onChange={filterProductTextChanged}
        ref={filterProductRef}
      />
      <button onClick={() => handleFilterProduct()}>Search</button>

      <div style={{ display: "flex", flexWrap: "wrap", gap: "8px" }}>
        {filteredProducts.map((product) => (
          <div key={product.id}
            style={{ padding: "8px", border: "2px grey solid", width: "160px" }}
          >
            <img
              src={product.image}
              alt={product.title}
              style={{ width: "100%" }}
            />
            <p style={{ fontSize: ".8rem" }}>
              <strong>{product.title}</strong>
            </p>
            <p style={{ fontSize: ".6rem" }}>{product.description}</p>
            <button
              style={{ width: "100%" }}
              onClick={() => {
                setLocation(`/products/${product.id}`);
              }}
            >
              View
            </button>
            <div style={{ margin: "8px" }} />
            <button
              style={{
                width: "100%",
                backgroundColor: "red",
                border: "1px solid red",
                color: "white",
              }}
              onClick={() => {
                dispatch({ type: "addToCart", product });
              }}
            >
              Add to Cart
            </button>
          </div>
        ))}
      </div>
    </>
  );
}
