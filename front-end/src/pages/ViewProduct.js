import React, { useEffect, useState } from "react";
import { RouteComponentProps } from "wouter";

const ViewProduct = ({ params }) => {
  const [product, setProduct] = useState();
  const [isLoading, setIsLoading] = useState(true);

  console.log(params.productId);

  useEffect(() => {
    fetch(`https://fakestoreapi.com/products/${params.productId}`)
      .then((res) => res.json())
      .then((json) => {
        setProduct(json);
        setIsLoading(false);
      });
  }, []);

  console.log(product);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <>
      <h2>View Product</h2>
      <div>
        <p>{product.id}</p>
        <p>{product.title}</p>
        <p>{product.price}</p>
        <p>{product.category}</p>
        <img src={product.image} alt={product.title} />
        <p>{product.rating.rate}</p>
        <p>{product.rating.count}</p>
      </div>
      {/* TODO implement viewing of product page. 
        The API of "Get a single product": https://fakestoreapi.com/docs */}
    </>
  );
};

export default ViewProduct;
